#!/bin/bash

# "to avoid continuing when errors or undefined variables are present"
set -eu

echo "Starting FTP Deploy"

[[ -z "${FTP_SERVER}" ]] && {
  echo "Missing variable FTP_SERVER. Exit" >2
  exit 1
}

[[ -z "${FTP_USERNAME}" ]] && {
  echo "Missing variable FTP_USERNAME. Exit" >2
  exit 1
}

[[ -z "${FTP_PASSWORD}" ]] && {
  echo "Missing variable FTP_PASSWORD. Exit" >2
  exit 1
}

transfer_local_dir=${LOCAL_DIR:-"/website"}
transfer_remote_dir=${REMOTE_DIR:-"."}
transfer_args=${ARGS:-""}
transfer_method=${METHOD:-"ftp"}

ls -al "${transfer_local_dir}"
cd "${transfer_local_dir}"
pwd
ls -al

if [ $transfer_method = "sftp" ]; then
  transfer_port=${PORT:-"22"}
  echo "Transfer method SFTP: establishing SFTP connection..."
  sshpass -p "$FTP_PASSWORD" sftp -o StrictHostKeyChecking=no -P "$transfer_port" "${FTP_USERNAME}@${FTP_SERVER}"
  echo "Connection established"
else
  echo "Transfer method FTP"
  transfer_port=${PORT:-"21"}
fi;

echo "Transfer ${transfer_local_dir} to ${FTP_SERVER} ${transfer_port} ${transfer_remote_dir}"

lftp "${transfer_method}://${FTP_SERVER}:${transfer_port}" -u "${FTP_USERNAME},${FTP_PASSWORD}" -e "set ftp:ssl-allow no; mirror ${transfer_args} -R ${transfer_local_dir} ${transfer_remote_dir}; quit"

transfer_exit_status="$?"

echo "FTP transfer complete ${transfer_exit_status}"
