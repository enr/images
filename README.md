# images

Container images build scripts and definitions.

Distributed images:

- `registry.gitlab.com/enr/images/ftp`: image containing `lftp` to transfer content via FTP.

Dockerfiles:

- `graal-ce`: Debian stretch with Graal VM Java 8.

## Usage

### FTP

Transfer via FTP the contents of the current working directory:

```
docker run -it --rm -v $PWD:/website/ \
       -e FTP_SERVER=$SERVER -e FTP_USERNAME=$USERNAME -e FTP_PASSWORD=$PASSWORD \
       registry.gitlab.com/enr/images/ftp:latest
```

Enter in the container:

```
docker run -it --entrypoint '/bin/bash' --rm registry.gitlab.com/enr/images/ftp:latest
```

## Development

All images are buildable from their directory, example:

```
cd ftp
docker build -f Dockerfile --rm=true --tag=myftp .
```

Note: If you are behind a web proxy, set the build variables for the build:

```
docker build --build-arg "https_proxy=..." --build-arg "http_proxy=..." --build-arg "no_proxy=..." ...
```
